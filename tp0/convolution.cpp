#include <opencv2/opencv.hpp>
#include <iostream>

// Fonction pour appliquer un filtre sur une image
cv::Mat applyFilter(const cv::Mat &inputImage, const cv::Mat &kernel) {
    // Creation d'une image de sortie
    cv::Mat outputImage = cv::Mat::zeros(inputImage.size(), inputImage.type());

    // Ajouter un padding a l'image d'entree
    int pad = kernel.rows / 2;
    cv::Mat paddedInput;
    cv::copyMakeBorder(inputImage, paddedInput, pad, pad, pad, pad, cv::BORDER_REPLICATE);

    // Operation de convolution
    for (int y = pad; y < paddedInput.rows - pad; ++y) {
        for (int x = pad; x < paddedInput.cols - pad; ++x) {
            double sum = 0.0;
            for (int k = -pad; k <= pad; ++k) {
                for (int j = -pad; j <= pad; ++j) {
                    sum += kernel.at<float>(k + pad, j + pad) * paddedInput.at<uchar>(y + k, x + j);
                }
            }
            outputImage.at<uchar>(y - pad, x - pad) = cv::saturate_cast<uchar>(sum);
        }
    }

    return outputImage;
}

int main() {
    // Charger l'image
    cv::Mat image = cv::imread("Lenna.png", cv::IMREAD_GRAYSCALE);
    if (image.empty()) {
        std::cout << "Impossible de charger l'image" << std::endl;
        return -1;
    }

    // Convolution kernel (example: 3x3 filtre gaussien)
    cv::Mat kernel = (cv::Mat_<float>(3,3) << 1/16.0, 2/16.0, 1/16.0,
                                           2/16.0, 4/16.0, 2/16.0,
                                           1/16.0, 2/16.0, 1/16.0);

    
    // Appliquer le filtre sur l'image
    cv::Mat filteredImage = applyFilter(image, kernel);
    
    // Afficher l'image originale et l'image filtree
    cv::namedWindow("Filtered Image", cv::WINDOW_AUTOSIZE);
    cv::imshow("Original Image", image);
    cv::imshow("Filtered Image", filteredImage);
    
    cv::waitKey(0);
    return 0;
}
