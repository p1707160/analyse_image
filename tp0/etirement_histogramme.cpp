#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

int main() {
    // Charger l'image
    cv::Mat image = cv::imread("Lenna.png", cv::IMREAD_GRAYSCALE);
    if (image.empty()) {
        std::cout << "Impossible de charger l'image" << std::endl;
        return -1;
    }

    // Definition des nouvelles intensites
    double a = 0;  
    double b = 255;

    // Trouver les intensites min et max
    double Nmin, Nmax;
    cv::minMaxLoc(image, &Nmin, &Nmax);

    // Etirer l'histogramme
    cv::Mat stretchedImage;
    image.convertTo(stretchedImage, CV_8U, (b - a) / (Nmax - Nmin), -Nmin * (b - a) / (Nmax - Nmin) + a);

    // Afficher l'image originale et l'image etiree
    cv::namedWindow("Original Image", cv::WINDOW_AUTOSIZE);
    cv::imshow("Original Image", image);
    cv::namedWindow("Stretched Image", cv::WINDOW_AUTOSIZE);
    cv::imshow("Stretched Image", stretchedImage);

    cv::waitKey(0);
    return 0;
}
