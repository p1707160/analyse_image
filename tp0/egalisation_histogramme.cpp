#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

int main() {
    // Charger l'image
    cv::Mat image = cv::imread("Lenna.png", cv::IMREAD_GRAYSCALE);
    if (image.empty()) {
        std::cout << "Impossible de charger l'image" << std::endl;
        return -1;
    }

    // Calculate the histogram
    int histSize = 256; // number of bins
    float range[] = { 0, 256 };
    const float* histRange = { range };
    cv::Mat hist;
    cv::calcHist(&image, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, true, false);

    // Compute the cumulative histogram
    cv::Mat cumHist = hist.clone();
    for (int i = 1; i < histSize; i++) {
        cumHist.at<float>(i) += cumHist.at<float>(i - 1);
    }

    // Normalize the cumulative histogram to the range [0, 1]
    cumHist /= (image.rows * image.cols);

    // Scale the cumulative histogram to the range [0, 255]
    cumHist *= 255;

    // Create the equalized image
    cv::Mat equalizedImage = image.clone();
    for (int y = 0; y < image.rows; y++) {
        for (int x = 0; x < image.cols; x++) {
            equalizedImage.at<uchar>(y, x) = cv::saturate_cast<uchar>(cumHist.at<float>(image.at<uchar>(y, x)));
        }
    }

    // Display the original and equalized images
    cv::namedWindow("Original Image", cv::WINDOW_AUTOSIZE);
    cv::imshow("Original Image", image);
    cv::namedWindow("Equalized Image", cv::WINDOW_AUTOSIZE);
    cv::imshow("Equalized Image", equalizedImage);

    cv::waitKey(0);
    return 0;
}
