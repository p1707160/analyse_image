#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

int main() {
    // Charger l'image
    cv::Mat image = cv::imread("Lenna.png", cv::IMREAD_GRAYSCALE);
    if (image.empty()) {
        std::cout << "Impossible de charger l'image" << std::endl;
        return -1;
    }

    // Calculate histogram
    int histSize = 256;
    float range[] = { 0, 256 }; //the upper boundary is exclusive
    const float* histRange = { range };
    cv::Mat hist;
    cv::calcHist(&image, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, true, false);

    // Normalize the histogram to probability values
    hist /= image.total();

    // Print normalized histogram values
    for(int i = 0; i < histSize; i++) {
        std::cout << "Gray Level " << i << " Probability: " << hist.at<float>(i) << std::endl;
    }

    // Assuming you want to visualize this probability histogram as well
    int hist_w = 512; 
    int hist_h = 400;
    int bin_w = cvRound((double)hist_w / histSize);
    cv::Mat histImage(hist_h, hist_w, CV_8UC1, cv::Scalar(0));

    // Draw the intensity line for histogram
    for (int i = 1; i < histSize; i++) {
        cv::line(histImage, cv::Point(bin_w*(i-1), hist_h - cvRound(hist.at<float>(i-1)*hist_h)),
                 cv::Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i)*hist_h)),
                 cv::Scalar(255), 2, 8, 0);
    }

    // Display
    cv::namedWindow("Source image", cv::WINDOW_AUTOSIZE);
    cv::imshow("Source image", image);
    cv::namedWindow("Histogram", cv::WINDOW_AUTOSIZE);
    cv::imshow("Histogram", histImage);

    cv::waitKey(0);
    return 0;
}
