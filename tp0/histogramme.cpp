
#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
using namespace std;
using namespace cv;

int main(int argc, char **argv)
{
    cv::Mat image = cv::imread("Lenna.png"); // Remplacer avec le chemin de votre image
    if (image.empty())
    {
        std::cout << "Erreur : Image non trouvée" << std::endl;
        return -1;
    }
    cv::imshow("Image Originale", image);

    cv::Mat grayImage;
    cv::cvtColor(image, grayImage, cv::COLOR_BGR2GRAY);
    cv::imshow("Image en Niveau de Gris", grayImage);

    // Séparation des canaux
    std::vector<cv::Mat> bgr_planes;
    cv::split(image, bgr_planes);

    // Nombre de bins pour l'histogramme
    int histSize = 256;

    // Plage des niveaux de gris
    float range[] = {0, 256};
    const float *histRange = {range};

    bool uniform = true, accumulate = false;
    cv::Mat b_hist, g_hist, r_hist;

    // Calcul des histogrammes pour chaque canal
    cv::calcHist(&bgr_planes[0], 1, 0, cv::Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate);
    cv::calcHist(&bgr_planes[1], 1, 0, cv::Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate);
    cv::calcHist(&bgr_planes[2], 1, 0, cv::Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate);

    // Dessiner les histogrammes pour chaque canal
    int hist_w = 512, hist_h = 400;
    int bin_w = cvRound((double)hist_w / histSize);

    cv::Mat histImage(hist_h, hist_w, CV_8UC3, cv::Scalar(0, 0, 0));

    // Normaliser les résultats à [ 0, histImage.rows ]
    cv::normalize(b_hist, b_hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat());
    cv::normalize(g_hist, g_hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat());
    cv::normalize(r_hist, r_hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat());

    // Dessiner pour chaque canal
    for (int i = 1; i < histSize; i++)
    {
        cv::line(histImage, cv::Point(bin_w * (i - 1), hist_h - cvRound(b_hist.at<float>(i - 1))),
                 cv::Point(bin_w * (i), hist_h - cvRound(b_hist.at<float>(i))),
                 cv::Scalar(255, 0, 0), 2, 8, 0);
        cv::line(histImage, cv::Point(bin_w * (i - 1), hist_h - cvRound(g_hist.at<float>(i - 1))),
                 cv::Point(bin_w * (i), hist_h - cvRound(g_hist.at<float>(i))),
                 cv::Scalar(0, 255, 0), 2, 8, 0);
        cv::line(histImage, cv::Point(bin_w * (i - 1), hist_h - cvRound(r_hist.at<float>(i - 1))),
                 cv::Point(bin_w * (i), hist_h - cvRound(r_hist.at<float>(i))),
                 cv::Scalar(0, 0, 255), 2, 8, 0);
    }

    cv::imshow("Histogramme Couleur", histImage);

    cv::Mat gray_hist;
    cv::calcHist(&grayImage, 1, 0, cv::Mat(), gray_hist, 1, &histSize, &histRange, uniform, accumulate);

    cv::Mat grayHistImage(hist_h, hist_w, CV_8UC3, cv::Scalar(0, 0, 0));
    cv::normalize(gray_hist, gray_hist, 0, grayHistImage.rows, cv::NORM_MINMAX, -1, cv::Mat());

    for (int i = 1; i < histSize; i++)
    {
        cv::line(grayHistImage, cv::Point(bin_w * (i - 1), hist_h - cvRound(gray_hist.at<float>(i - 1))),
                 cv::Point(bin_w * (i), hist_h - cvRound(gray_hist.at<float>(i))),
                 cv::Scalar(255, 255, 255), 2, 8, 0);
    }

    cv::imshow("Histogramme Niveau de Gris", grayHistImage);
    cv::waitKey(0);
}
