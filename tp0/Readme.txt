Travail réalisé par : TEMIRBOULATOV KOUREICH P1707160

L'archive contient : 

- un fichier CMakeLists.txt contenant les règles d'execution pour les différents fichiers sources.

- main.cpp regroupe toutes les parties du tp0 en un seul fichier executable.

- histogramme.cpp permet d'afficher les histogramme basiques pour une image en couleur et en noir & blanc.

- etirement_histogramme.cpp permet d'afficher l'image original ainsi que l'image ayant subi l'étirement ainsi que son histogramme.

- normaliser_histogramme.cpp permet d'afficher l'image original en noir & blanc ainsi que son histogramme normaliser.

- egalisation_histogramme.cpp permet d'afficher deux images : l'original et celle ayant subi la normalisation.

- convolution.cpp permet d'afficher deux images : l'original et celle ayant subi le filtre, ici le flou gaussien a été appliquer.

- Une image png "Lenna.png".

Pour compiler le projet faire les commandes suivantes depuis la racine de l'archive : 

$ cmake .

$ make

puis lancer les executables généré à la compilation : 
$ ./main ou ./histogramme ou ./normaliser_histogramme ou ./etirement_histogramme ou ./egalisation_histogramme ou ./convolution
