#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <vector>

// Fonction pour calculer l'histogramme d'une image
cv::Mat calculateHistogram(const cv::Mat& image) {
    // Parametres pour l'histogramme
    int histSize = 256;
    float range[] = { 0, 256 };
    const float* histRange = { range };
    cv::Mat hist;
    cv::calcHist(&image, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, true, false);

    // Dessiner l'histogramme
    int histWidth = 512, histHeight = 400;
    int binWidth = cvRound((double) histWidth / histSize);
    cv::Mat histImage(histHeight, histWidth, CV_8UC1, cv::Scalar(0));
    cv::normalize(hist, hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat());

    for(int i = 1; i < histSize; i++) {
        cv::line(histImage, cv::Point(binWidth * (i - 1), histHeight - cvRound(hist.at<float>(i - 1))),
                 cv::Point(binWidth * i, histHeight - cvRound(hist.at<float>(i))),
                 cv::Scalar(255), 2, 8, 0);
    }

    return histImage;
}

// Fonction pour etirer l'histogramme d'une image
cv::Mat stretchHistogram(const cv::Mat& image) {
    double minVal, maxVal;
    cv::minMaxLoc(image, &minVal, &maxVal);
    cv::Mat stretched;
    image.convertTo(stretched, CV_8U, 255.0 / (maxVal - minVal), -minVal * 255.0 / (maxVal - minVal));
    return stretched;
}

// Fonction pour egaliser l'histogramme d'une image
cv::Mat equalizeHistogram(const cv::Mat& image) {
    cv::Mat equalized;
    cv::equalizeHist(image, equalized);
    return equalized;
}

// Fonction pour appliquer une convolution sur une image
cv::Mat applyConvolution(const cv::Mat& image, const cv::Mat& kernel) {
    cv::Mat output;
    cv::filter2D(image, output, CV_8U, kernel, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
    return output;
}

// Fonction pour appliquer une convolution sur un histogramme
cv::Mat convolutedHistogram(const cv::Mat& image, const cv::Mat& kernel) {
    cv::Mat output;
    cv::filter2D(image, output, CV_8U, kernel, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
    return output;
}

int main() {
    // Charger l'image originale en niveaux de gris
    cv::Mat image = cv::imread("Lenna.png", cv::IMREAD_GRAYSCALE);
    if (image.empty()) {
        std::cout << "Could not open or find the image" << std::endl;
        return -1;
    }

    cv::imshow("Original Image", image);

    // Afficher l'histogramme de l'image originale
    cv::Mat histImage = calculateHistogram(image);
    cv::imshow("Histogram", histImage);

    // Etirement de l'histogramme
    cv::Mat stretchedImage = stretchHistogram(image);
    cv::imshow("Stretched Histogram", calculateHistogram(stretchedImage));
    cv::imshow("Stretched Image", stretchedImage);

    // Egalisation de l'histogramme
    cv::Mat equalizedImage = equalizeHistogram(image);
    cv::imshow("Equalized Histogram", calculateHistogram(equalizedImage));
    cv::imshow("Equalized Image", equalizedImage);

    // Convolution kernel (example: 3x3 filtre gaussien)
    cv::Mat kernel = (cv::Mat_<float>(3,3) << 1/16.0, 2/16.0, 1/16.0,
                                             2/16.0, 4/16.0, 2/16.0,
                                             1/16.0, 2/16.0, 1/16.0);
    

    // Appliquer le filtre et afficher l'image filtrée
    cv::Mat convolutedImage = applyConvolution(image, kernel);
    cv::imshow("Convoluted Histogram", calculateHistogram(convolutedImage));
    cv::imshow("Convoluted Image", convolutedImage);

    cv::waitKey(0);
    return 0;
}
